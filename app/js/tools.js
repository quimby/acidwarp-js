let state,
    paletteElem,
    frameRateElem,
    paletteChooser,
    pictureChooser,
    speedGroup,
    lockPalette,
    lockPicture,
    toggleTools,
    container;

exports.init = function (stateObj) {
    state = stateObj;
    state.addListener(onStateChange);

    paletteElem = document.getElementById('palette');
    frameRateElem = document.getElementById('frameRate');

    paletteChooser = document.getElementById('paletteChooser');
    initPalettes();

    pictureChooser = document.getElementById('pictureChooser');
    initPictures();

    speedGroup = document.getElementById('speed');
    initSpeed();

    lockPalette = document.getElementById('lockPalette');
    lockPalette.addEventListener('change', function(e) {
        state.lockPalette = this.checked;
    });

    lockPicture = document.getElementById('lockPicture');
    lockPicture.addEventListener('change', function(e) {
        state.lockPicture = this.checked;
    });

    container = document.getElementById('container');
    toggleTools = document.getElementById('toggleTools');
    initToggleTools();
};

function initPalDom() {
    for (let i = 0; i < 256; i++) {
        let div = document.createElement('div');
        div.className = 'pal_color';
        paletteElem.appendChild( div );
    }
}

function initPalettes() {
    let descriptions = state.paletteDescriptions;
    for (let i=0; i<descriptions.length; i++) {
        let opt = document.createElement('option');
        opt.setAttribute('value', i);
        opt.textContent = descriptions[i];
        paletteChooser.appendChild(opt);
    }
    paletteChooser.value = state.palette;
    paletteChooser.addEventListener('change', e => {
        state.palette = parseInt(paletteChooser.value, 10);
    });
}

function initPictures() {
    let descriptions = state.pictureDescriptions;
    for (let i=0; i<descriptions.length; i++) {
        let opt = document.createElement('option');
        opt.setAttribute('value', i);
        opt.textContent = descriptions[i];
        pictureChooser.appendChild(opt);
    }
    pictureChooser.value = state.picture;
    pictureChooser.addEventListener('change', e => {
        state.picture = parseInt(pictureChooser.value);
    });
}

function initSpeed() {
    setSpeedIndex(state.speedIndex);

    speedGroup.addEventListener('click', e => {
        let button = e.target;
        if (button.tagName === 'BUTTON') {
            let speedIndex = 0;
            let elem = button;
            while (elem.previousSibling) {
                elem = elem.previousSibling;
                if (elem.tagName === 'BUTTON') {
                    speedIndex++;
                }
            }
            state.speedIndex = speedIndex;
        }
    });
}

function setSpeedIndex(speedIndex) {
    let buttons = speedGroup.getElementsByTagName('button');
    for (let i=0; i<buttons.length; i++) {
        let b = buttons.item(i);
        if (i === speedIndex) {
            b.classList.add('active');
        } else {
            b.classList.remove('active');
        }
    }
}

function initToggleTools() {
    setToolsVisibility(state.toolsVisible);
    toggleTools.addEventListener('click', function(e) {
        e.preventDefault();
        let newVal = !container.classList.contains('toolsVisible');
        state.toolsVisible = newVal;
        setToolsVisibility(newVal);
    });
}

function setToolsVisibility(on) {
    if (on) {
        container.classList.add('toolsVisible');
        toggleTools.textContent = 'Hide Tools';
    } else {
        container.classList.remove('toolsVisible');
        toggleTools.textContent = 'Show Tools';
    }
}

function onStateChange(e) {
    // console.log('tools.onStateChange', e);
    if (e.which === 'speedIndex') {
        setSpeedIndex(e.value);
    } else if (e.which === 'palette') {
        paletteChooser.value = e.value;
    } else if (e.which === 'picture') {
        pictureChooser.value = e.value;
    }
}

exports.update = function(palette) {
    showPalette(palette);
    showFramerate();
};

function showPalette(pal) {
    let elem = paletteElem.firstChild;
    if (!elem) {
        initPalDom();
        elem = paletteElem.firstChild;
    }

    let i=0;
    while (elem) {
        let color = pal[i];
        elem.style.backgroundColor = 'rgb(' + color.r + ',' + color.g + ',' + color.b + ')';
        i++;
        elem = elem.nextSibling;
    }
}

function showFramerate() {
    frameRateElem.textContent = state.frameRate ? (state.frameRate + ' FPS') : '';
}
