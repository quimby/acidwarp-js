
/* palette and image providers */
let paletteBuilder = require('./palette');
let pictureBuilder = require('./picture');

/* shared state */
let state = require('./state').create(paletteBuilder.descriptions(), pictureBuilder.descriptions());
state.addListener(onStateChange);

/* tools panel */
let tools = require('./tools');
tools.init(state);

/* rendering support */
let utils = require('./utils');
let canvas = document.getElementById('canvas');
let renderer = require('./render').create(canvas);
pictureBuilder.setDimensions(canvas.width, canvas.height);

let rollAndFade = require('./rollAndFade');
rollAndFade.setWorkingPalette(paletteBuilder.workingPalette());

/* local state */
let palette, picture;
let sleepTimer = undefined;
let randomSceneEndTimeMs = -1;

const ROLL = 0;
const ROLL_AND_FADE_OUT = 1;
const ROLL_AND_FADE_TO_TARGET = 2;
const ROLL_AND_FADE_TO_RANDOM_TARGET = 3;

const FADE_COLORS = [utils.BLACK, utils.WHITE];

let processNum = ROLL_AND_FADE_TO_TARGET;

function onStateChange(e) {
    // console.log("acidwarp.onStateChange", e);
    if (e.which === 'speedIndex') {
        if (state.sleepTime && !sleepTimer) {
            startTimer();
        }
    } else if (e.which === 'palette') {
        initPalette(e.value);
    } else if (e.which === 'picture') {
        initPicture(e.value);
    }
}

exports.start = function() {

    initPalette(state.palette);
    initPicture(state.picture);

    paint();

    if (state.sleepTime) {
        startTimer();
    }
};

function initPalette(id) {
    let pal = paletteBuilder.buildPalette(id);
    rollAndFade.setTargetPalette(pal);
    setTargetPaletteMode(); // in case it's called from user interface changes.
    initRandomSceneEnd();
}

function initPicture(id) {
    picture = pictureBuilder.buildPicture(id);
    initRandomSceneEnd();
    if (!state.sleepTime) {
        paint();
    }
}

function initRandomSceneEnd() {
    if (state.randomMode) {
        randomSceneEndTimeMs = new Date().getTime() + state.randomSceneDurationSecs * 1000;
    } else {
        randomSceneEndTimeMs = -1;
    }
}

function isRandomSceneEndReached() {
    return state.randomMode && new Date().getTime() > randomSceneEndTimeMs;
}

function evolve() {
    if (processNum === ROLL_AND_FADE_TO_TARGET) {
        if (rollAndFade.rollAndFadeTarget()) {
            console.log("-> ROLL");
            processNum = ROLL;
        }
    } else if (processNum === ROLL_AND_FADE_TO_RANDOM_TARGET) {
        if (rollAndFade.rollAndFadeTarget()) {
            console.log("-> NEXT_RANDOM_TARGET");
            // init next random target palette
            let pal = paletteBuilder.buildRealRandomPalette();
            rollAndFade.setTargetPalette(pal);
        }
        if (isRandomSceneEndReached()) {
            handleRandomSceneEnd();
        }
    } else if (processNum === ROLL_AND_FADE_OUT) {
        if (rollAndFade.rollAndFadeOut()) {
            state.initRandomScene();
            setTargetPaletteMode();
        }
    } else {
        rollAndFade.roll(palette);
        if (isRandomSceneEndReached()) {
            handleRandomSceneEnd();
        }
    }
}

function handleRandomSceneEnd() {
    if (state.lockPicture) {
        // we don't fade out when the picture is locked
        state.initRandomScene();
        setTargetPaletteMode();
    } else {
        // Noahs face always fade to black - otherwise random
        rollAndFade.setFadeColor(state.picture === 0 ? utils.BLACK : FADE_COLORS[utils.random(FADE_COLORS.length)]);
        processNum = ROLL_AND_FADE_OUT;
        console.log("-> ROLL_AND_FADE_OUT");
    }
}

function setTargetPaletteMode() {
    if (paletteBuilder.isMorphingPalette(state.palette)) {
        processNum = ROLL_AND_FADE_TO_RANDOM_TARGET;
        console.log("-> ROLL_AND_FADE_TO_RANDOM_TARGET");
    } else {
        processNum = ROLL_AND_FADE_TO_TARGET;
        console.log("-> ROLL_AND_FADE_TO_TARGET");
    }
}

function paint() {
    let pal = rollAndFade.getWorkingPalette();
    renderer.render(picture, pal);
    if (state.toolsVisible) {
        tools.update(pal);
    }
}

function startTimer() {
    sleepTimer = window.setTimeout(function () {
        sleepTimer = undefined;

        evolve();
        paint();
        state.gatherStatistics();

        if (state.sleepTime) {
            startTimer();
        }
    }, state.sleepTime);
}
