exports.create = function(canvas) {
    let ctx = canvas.getContext('2d');
    let imageData = ctx.createImageData(canvas.width, canvas.height);

    function render(picture, palette) {
        let data = imageData.data;
        let j = 0;
        for (let i=0; i<picture.length; i++) {
            let color = palette[picture[i]];
            // console.log(j, color.r, color.g, color.b);
            data[j++] = color.r;
            data[j++] = color.g;
            data[j++] = color.b;
            data[j++] = 255;    // alpha
        }
        ctx.putImageData(imageData, 0, 0);
    }

    return {
        render: render
    }
};
