let utils = require('./utils');

function init_working_palette() {
    let palette = new Array(256);
    for (let i = 0; i < 256; i++) {
        palette[i] = utils.color(0, 0, 0);
    }
    return palette;
}

function init_rgbw_palette() {
    let palette = [];
    for (let i = 0; i < 32; i++) {
        let comp = 4 * (i * 2);
        palette[i] = utils.color(comp, 0, 0);
        palette[i + 64] = utils.color(0, comp, 0);
        palette[i + 128] = utils.color(0, 0, comp);
        palette[i + 192] = utils.color(comp, comp, comp);
    }

    for (let i = 32; i < 64; i++) {
        let comp = 4 * ((63 - i) * 2);
        palette[i] = utils.color(comp, 0, 0);
        palette[i + 64] = utils.color(0, comp, 0);
        palette[i + 128] = utils.color(0, 0, comp);
        palette[i + 192] = utils.color(comp, comp, comp);
    }
    return palette;
}

function init_white_palette() {
    let palette = [];
    for (let i = 0; i < 128; i++) {
        let comp = 4 * Math.floor(i / 2);
        palette[i] = utils.color(comp, comp, comp);
    }
    for (let i = 128; i < 256; i++) {
        let comp = 4 * Math.floor((255 - i) / 2);
        palette[i] = utils.color(comp, comp, comp);
    }
    return palette;
}

function init_white_half_palette() {
    let palette = [];
    for (let i = 0; i < 64; i++) {
        let comp = 4 * i;
        palette[i] = utils.color(comp, comp, comp);
        comp = 4 * (63 - i);
        palette[i + 64] = utils.color(comp, comp, comp);
    }
    for (let i = 128; i < 256; i++) {
        palette[i] = utils.color(0, 0, 0);
    }
    return palette;
}

function init_pastel_palette() {
    let palette = [];
    for (let i = 0; i < 128; i++) {
        let comp = (31 + Math.floor(i/4)) * 4;
        palette[i] = utils.color(comp, comp, comp);

        comp = (31 + Math.floor((127 - i)/4)) * 4;
        palette[i+128] = utils.color(comp, comp, comp);
    }
    return palette;
}

function add_sparkles(palette) {
    for (let i = 1; i < 256; i += 4) {
        let c = palette[i];
        c.r = Math.min(255, c.r + 36);
        c.g = Math.min(255, c.g + 36);
        c.b = Math.min(255, c.b + 36);
    }
    return palette;
}

let palettes = [
    {
        name: 'Sharp palette with four colored bands',
        produce: init_rgbw_palette
    },
    {
        name: 'Smooth white palette',
        produce: init_white_palette
    },
    {
        name: 'Bolder smooth white palette',
        produce: init_white_half_palette
    },
    {
        name: 'Pastel palette',
        produce: init_pastel_palette
    },
    {
        name: 'Sharp palette with sparkles',
        produce: function () {
            return add_sparkles(init_rgbw_palette());
        }
    },
    {
        name: 'Smooth white palette with sparkles',
        produce: function () {
            return add_sparkles(init_white_palette());
        }
    },
    {
        name: 'Bolder smooth white palette with sparkles',
        produce: function () {
            return add_sparkles(init_white_half_palette());
        }
    },
    {
        name: 'Pastel palette with sparkles',
        produce: function () {
            return add_sparkles(init_pastel_palette());
        }
    },
    {
        name: 'Random morphing palette',
        produce: function () {
            return buildRealRandomPalette();
        }
    }
];

exports.descriptions = function(i) {
    return palettes.map(f => f.name);
};

exports.buildPalette = function(i) {
    return palettes[i].produce();
};

exports.workingPalette = init_working_palette;

exports.isMorphingPalette = function(i) {
    return i === (palettes.length - 1);
};

/* Builds one of the real palettes (not the morphing one). */
function buildRealRandomPalette() {
    let palIndex = utils.random(palettes.length - 1);
    return palettes[palIndex].produce();
}

exports.buildRealRandomPalette = buildRealRandomPalette;