# Acidwarp JS

This as a Javascript/HTML canvas port of Acidwarp (http://www.noah.org/acidwarp/), originally written by Noah Spurrier in 1992 for DOS. See below for the original Acidwarp README.

Watch Acidwarp JS in action on: http://quimby.de/acidwarp/.

Acidwarp generates patterns based on mathematical functions and animates them to a trippy effect using the ancient art of color cycling (or palette rotation).

There are about 40 different patterns shown in random order and changed every 20 seconds. The patterns are animated using 8 different color palettes that are also chosen randomly. In addition, there is a special palette which is constantly morphing between the other palettes. 

A small tool panel is available, which allows you to choose specific patterns and palettes. Lock a pattern or a palette if you want it to run indefinitely. You may also change the speed and view the palette colors as they are changing.

## Color cycling

When working on Acidwarp JS I discovered this article [Old School Color Cycling with HTML5](http://www.effectgames.com/effect/article-Old_School_Color_Cycling_with_HTML5.html) by Joseph Huckaby who also ported some very nice legacy color cycling game sceneries to HTML canvas  - I borrowed the idea to live display the current palette from him. You'll find some historical background on the technique there and also an interview with a game designer who created some amazing artwork using color cycling.

## Technical notes

For the original version of Acidwarp some effort was put into making the program performant on the hardware of that time. The animation was achieved by directly manipulating the VGA palette colors - no need to repaint the modified images for each animation frame. And instead of math functions (to calculate the patterns) it used pregenerated lookup tables for trigonometric calculations. 

HTML canvas doesn't support index based color models, so I had to emulate the color lookup and repaint the canvas all the time. Which amounts to quite an amount of calculations in an RGB color model - even for small canvases. I didn't expect it to be so smooth, but even my 3 year old phone holds up well.

Please feel free to check out the code (https://gitlab.com/quimby/acidwarp-js). I'm using Webpack for modularization and bundling. I threw in Babel just in case, although I'm not using any fancy ES6 features. It seems to run decently in most halfway recent browsers I tried, back to IE10. If you encounter any problems, don't hesitate to tell me.

If you want to build Acidwarp yourself, check it out from Gitlab and run the usual invocations (nodejs and npm are required):

```
npm install
npm run build
```

or for a live-reloading development experience:

```
npm run serve
```


## Original Readme

> Acid Warp V4.2 By Noah Spurrier
>
> This is Acid Warp for DOS. It's an eye-candy program. Run it and watch the
> pretty colors. Each picture is the graphic representation of a mathematical
> function. All math is done with integer approximations so Acid Warp should
> be pretty fast even on machines without a math chip. It might take as long
> as 30 to 40 seconds to generate each picture on a slow machine.
>
> I added a few new picture functions (now there are over 40) and I improved
> the color palettes so that they seem more hypnotic. Don't forget to try
> Acid Warp with the "p8" option. This forces all the pictures to be very
> trippy. The default is to mix mellow and trippy pictures. Also don't forget
> the "w" option which displays a document that describes how to turn your
> monitor into a wall projector for Acid Warp.
>
> Sorry this version took so long. I didn't expect people to like Acid Warp
> so much. I had a lot of help from Mark Bilk in fixing this code up. He pretty
> much redid all the interrupt routines so they should be more stable now and
> he figured out how to get rid of the screen flicker that annoyed about 1/3
> of the VGA cards that we tried it on. This was the biggest complaint about
> the old version. It should be fixed now. Hooray!
>
> Sorry, but the make file is in a bit of a mess. The VC++70.MAK file
> should work for older versions of Visual C++, but I have not had the chance
> to test it. This source was written for Turbo C, then for Microsoft C++ 7.0.
> I have not bothered to convert it to VC++ 2.2.
> All the source files should be in a readable condition.
>
> This is free software, but if you happen to have any weird information to
> send me as a token of your appreciation then mail it to "noah@noah.org".
> I'd really like a copy of Amanda Fielding's film on Trepanning called
> "Heartbeat in the Brain".

