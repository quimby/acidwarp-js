/* This used to be Lookup Tables and is now using math,
 * which doesn't seem to be a problem nowadays.
 */

const ANGLE_UNIT = 256;
const ANGLE_UNIT_HALF = ANGLE_UNIT / 2;
const ANGLE_UNIT_QUART = ANGLE_UNIT / 4;

/** distance */
function dist(x, y) {
    return Math.round(Math.sqrt(x*x + y*y));
}

/** sin
 * input normalized to: (0) - (255)
 * returns (-512) - (512)
 */
function sin(angle) {
    if (angle < 0) {
        angle = -angle + ANGLE_UNIT_HALF;
    }
    angle %= ANGLE_UNIT;

    return Math.round(Math.sin(angle / ANGLE_UNIT_HALF * Math.PI) * ANGLE_UNIT * 2);
}

/** cos */
function cos(angle) {
    return sin(angle + ANGLE_UNIT_QUART);
}

/** angle
 *  output: 0 - ANGLE_UNIT
 */
function angle(x, y) {
    let angle = Math.atan2(y,x) / Math.PI * ANGLE_UNIT_HALF;
    if (angle < 0) {
        angle += ANGLE_UNIT;
    }
    return Math.floor(angle);
}

module.exports = {
    cos: cos,
    sin: sin,
    dist: dist,
    angle: angle,
    ANGLE_UNIT: ANGLE_UNIT
};
