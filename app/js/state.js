let utils = require('./utils');

/* Holds the application state.
 * Binds the tools panel options to acidwarp.
 */

const HIGH_SPEED = 20;
const MEDIUM_SPEED = 40;
const LOW_SPEED = 80;
const SPEED_OFF = 0;

const SPEEDS = [HIGH_SPEED, MEDIUM_SPEED, LOW_SPEED, SPEED_OFF];

exports.create = function(paletteDescriptions, pictureDescriptions) {
    return {
        paletteDescriptions: paletteDescriptions,
        pictureDescriptions: pictureDescriptions,

        toolsVisible: false,

        // ------------------
        // current roll speed - initial value: MEDIUM.
        _speedIndex: 1,

        get speedIndex() {
            return this._speedIndex;
        },

        set speedIndex(value) {
            if (value !== this._speedIndex) {
                this._resetStatistics();
                this._speedIndex = value;
                this._emitEvent('speedIndex', this._speedIndex);
            }
        },

        get sleepTime() {
            return SPEEDS[Math.min(SPEEDS.length - 1, this._speedIndex)];
        },

        // ------------------
        // scene lock and randomization
        randomSceneDurationSecs: 20,

        get randomMode() {
            return !(this._lockPalette && this._lockPicture);
        },

        _lockPalette: false,

        get lockPalette() {
            return this._lockPalette;
        },

        set lockPalette(value) {
            if (value !== this._lockPalette) {
                this._lockPalette = value;
                this._emitEvent('lockPalette', this._lockPalette);
            }
        },

        _lockPicture: false,

        get lockPicture() {
            return this._lockPicture;
        },

        set lockPicture(value) {
            if (value !== this._lockPicture) {
                this._lockPicture= value;
                this._emitEvent('lockPicture', this._lockPicture);
            }
        },

        initRandomScene: function() {
            if (!this.lockPalette) {
                let nextPalette = this._palette;
                while (nextPalette === this._palette) {
                    nextPalette = utils.random(this.paletteDescriptions.length);
                }
                this.palette = nextPalette;
            }
            if (!this.lockPicture) {
                let nextPicture = this._picture;
                while (nextPicture === this._picture) {
                    // Noahs face (index = 0) is not eligible. It's only shown during start.
                    nextPicture = utils.random(this.pictureDescriptions.length - 1) + 1;
                }
                this.picture = nextPicture;
            }
        },

        // ------------------
        // picture - initial value: Noah's face.
        _picture: 0,

        get picture() {
            return this._picture;
        },

        set picture(value) {
            if (value !== this._picture) {
                this._picture = value;
                this._emitEvent('picture', this._picture);
            }
        },

        // ------------------
        // palette - initial value: sharp palette with sparkles (preset for Noah's face).
        _palette: 4,

        get palette() {
            return this._palette;
        },

        set palette(value) {
            if (value !== this._palette) {
                this._palette = value;
                this._emitEvent('palette', this._palette);
            }
        },

        // ------------------
        // event handling
        _listeners: [],

        addListener: function(callback) {
            if (callback) {
                this._listeners.push(callback);
            }
        },

        _emitEvent(name, value) {
            let event = {
                target: this,
                which: name,
                value: value
            };
            this._listeners.forEach(callback => {
                try {
                    callback(event);
                } catch(e) {
                    console.error('failed to dispatch event', e);
                }
            });
        },

        // ------------------
        // statistics
        _lastMillis: 0,
        _numSamples: 0,
        frameRate: 0,

        _resetStatistics() {
            this._lastMillis = 0;
            this._numSamples = 0;
        },

        /** called every frame, calculates frame rate */
        gatherStatistics() {
            let millis = new Date().getTime();
            if (this._lastMillis) {
                let currentRate = Math.round(1000 / (millis - this._lastMillis));
                // console.log(this.lastMillis, millis, (millis - this.lastMillis), currentRate);
                if (this._numSamples) {
                    this.frameRate = Math.round((this.frameRate * this._numSamples + currentRate) / (this._numSamples + 1));
                    if (this._numSamples < 50) {
                        this._numSamples++;
                    }
                } else {
                    this.frameRate = currentRate;
                    this._numSamples = 1;
                }
            }
            this._lastMillis = millis;
        }
    };
};
