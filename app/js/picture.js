let Lut = require('./lut');
let utils = require('./utils');

const ANGLE_UNIT = Lut.ANGLE_UNIT;

let width = 0,
    height = 0;

exports.setDimensions = function(w, h) {
    width = w;
    height = h;
};

let pictures = [
    {   name: "Noah's face",
        producePicture: noahsFace
    },
    {   name: "Rays plus 2D Waves",
        producePixel: function() {
            return this.angle
                + Math.floor(Lut.sin(this.dist * 10) / 64)
                + Math.floor(Lut.cos(Math.floor(this.x * ANGLE_UNIT / this.width * 2)) / 32)
                + Math.floor(Lut.cos(Math.floor(this.y * ANGLE_UNIT / this.height * 2)) / 32);
        }
    },
    {   name: "Rays plus 2D Waves (2)",
        producePixel: function() {
            return this.angle
                + Math.floor(Lut.sin(this.dist * 10) / 16)
                + Math.floor(Lut.cos(Math.floor(this.x * ANGLE_UNIT / this.width * 2)) / 8)
                + Math.floor(Lut.cos(Math.floor(this.y * ANGLE_UNIT / this.height * 2)) / 8);
        }
    },
    {   name: "Trippy tunnel",
        producePixel: function() {
            return Math.floor(Lut.sin(Lut.dist(this.dx + this.x1, this.dy + this.y1) *  4) / 32)
                + Math.floor(Lut.sin(Lut.dist(this.dx + this.x2, this.dy + this.y2) *  8) / 32)
                + Math.floor(Lut.sin(Lut.dist(this.dx + this.x3, this.dy + this.y3) * 16) / 32)
                + Math.floor(Lut.sin(Lut.dist(this.dx + this.x4, this.dy + this.y4) * 32) / 32);
        }
    },
    {   name: "Peacock",
        producePixel: function() {
            return this.angle + Math.floor(Lut.sin(Lut.dist(this.dx + 20, this.dy) * 10) / 32)
                + this.angle + Math.floor(Lut.sin(Lut.dist(this.dx - 20, this.dy) * 10) / 32);
        }
    },
    {   name: "Spin dryer",
        producePixel: function() {
            return Math.floor(Lut.sin(this.dist) / 16);
        }
    },
    {   name: "2D Wave + Spiral",
        producePixel: function() {
            return Math.floor(Lut.cos(Math.floor(this.x * ANGLE_UNIT / this.width)) / 8)
                 + Math.floor(Lut.cos(Math.floor(this.y * ANGLE_UNIT / this.height)) / 8)
                 + this.angle + Math.floor(Lut.sin(this.dist) / 32);
        }
    },
    {   name: "Peacock, 3 centers", // "Monkey face"
        producePixel: function() {
            return Math.floor(Lut.sin(Lut.dist(this.dx, this.dy - 20) * 4) / 32)
                 + Math.floor(Lut.sin(Lut.dist(this.dx + 20, this.dy + 20) * 4) / 32)
                 + Math.floor(Lut.sin(Lut.dist(this.dx - 20, this.dy + 20) * 4) / 32);
        }
    },
    {   name: "Peacock, 3 centers (2)",
        producePixel: function() {
            return Math.floor(Lut.sin(Lut.dist(this.dx, this.dy - 20) * 8) / 32)
                + Math.floor(Lut.sin(Lut.dist(this.dx + 20, this.dy + 20) * 8) / 32)
                + Math.floor(Lut.sin(Lut.dist(this.dx - 20, this.dy + 20) * 8) / 32);
        }
    },
    {   name: "Peacock, 3 centers (3)",
        producePixel: function() {
            return Math.floor(Lut.sin(Lut.dist(this.dx, this.dy - 20) * 12) / 32)
                + Math.floor(Lut.sin(Lut.dist(this.dx + 20, this.dy + 20) * 12) / 32)
                + Math.floor(Lut.sin(Lut.dist(this.dx - 20, this.dy + 20) * 12) / 32);
        }
    },
    {   name: "Five Arm Star",
        producePixel: function() {
            return this.dist + Math.floor(Lut.sin(5 * this.angle) / 64);
        }
    },
    {   name: "2D wave",
        producePixel: function() {
            return Math.floor(Lut.cos(Math.floor(this.x * ANGLE_UNIT / this.width) * 2) / 4)
                + Math.floor(Lut.cos(Math.floor(this.y * ANGLE_UNIT / this.height) * 2) / 4);
        }
    },
    {   name: "2D wave (2)",
        producePixel: function() {
            return Math.floor(Lut.cos(Math.floor(this.x * ANGLE_UNIT / this.width)) / 8)
                 + Math.floor(Lut.cos(Math.floor(this.y * ANGLE_UNIT / this.height)) / 8);
        }
    },
    {   name: "Concentric Rings",
        producePixel: function() {
            return this.dist;
        }
    },
    {   name: "Simple Rays",
        producePixel: function() {
            return this.angle;
        }
    },
    {   name: "Toothed Spiral Sharp",
        producePixel: function() {
            return this.angle + Math.floor(Lut.sin(this.dist * 8) / 32);
        }
    },
    {   name: "Rings with sine",
        producePixel: function() {
            return Math.floor(Lut.sin(this.dist * 4) / 32);
        }
    },
    {   name: "Rings with sine with sliding inner rings",
        producePixel: function() {
            return this.dist + Math.floor(Lut.sin(this.dist * 4) / 32);
        }
    },
    {   name: "Table cloth",
        producePixel: function() {
            return Math.floor(Lut.sin(Lut.cos(Math.floor(2 * this.x * ANGLE_UNIT / this.width))) / (20 + this.dist))
                 + Math.floor(Lut.sin(Lut.cos(Math.floor(2 * this.y * ANGLE_UNIT / this.height))) / (20 + this.dist));
        }
    },
    {   name: "Lattice",
        producePixel: function() {
            return Math.floor(Lut.cos(Math.floor(7 * this.x * ANGLE_UNIT / this.width)) / (20 + this.dist))
                 + Math.floor(Lut.cos(Math.floor(7 * this.y * ANGLE_UNIT / this.height)) / (20 + this.dist));
        }
    },
    {   name: "Small lattice",
        producePixel: function() {
            return Math.floor(Lut.cos(Math.floor(17 * this.x * ANGLE_UNIT / this.width)) / (20 + this.dist))
                 + Math.floor(Lut.cos(Math.floor(17 * this.y * ANGLE_UNIT / this.height)) / (20 + this.dist));
        }
    },
    {   name: "Spiral lattice",
        producePixel: function() {
            return Math.floor(Lut.cos(Math.floor(17 * this.x * ANGLE_UNIT / this.width)) / 32)
                 + Math.floor(Lut.cos(Math.floor(17 * this.y * ANGLE_UNIT / this.height)) / 32)
                 + this.dist + this.angle;
        }
    },
    {   name: "Melted lattice",
        producePixel: function() {
            return Math.floor(Lut.cos(Math.floor(7 * this.x * ANGLE_UNIT / this.width)) / 32)
                 + Math.floor(Lut.cos(Math.floor(7 * this.y * ANGLE_UNIT / this.height)) / 32)
                 + this.dist;
        }
    },
    {   name: "Double lattice",
        producePixel: function() {
            return Math.floor(Lut.cos(Math.floor(7 * this.x * ANGLE_UNIT / this.width)) / 32)
                 + Math.floor(Lut.cos(Math.floor(7 * this.y * ANGLE_UNIT / this.height)) / 32)
                 + Math.floor(Lut.cos(Math.floor(11 * this.x * ANGLE_UNIT / this.width)) / 32)
                 + Math.floor(Lut.cos(Math.floor(11 * this.y * ANGLE_UNIT / this.height)) / 32);
        }
    },
    {   name: "Glorious rays",
        producePixel: function() {
            return Math.floor(Lut.sin(this.angle * 7) / 32);
        }
    },
    {   name: "Swirl",
        producePixel: function() {
            return Math.floor(Lut.sin(Lut.dist(this.dx + this.x1, this.dy + this.y1) * 2) / 12)
                 + Math.floor(Lut.sin(Lut.dist(this.dx + this.x2, this.dy + this.y2) * 4) / 12)
                 + Math.floor(Lut.sin(Lut.dist(this.dx + this.x3, this.dy + this.y3) * 6) / 12)
                 + Math.floor(Lut.sin(Lut.dist(this.dx + this.x4, this.dy + this.y4) * 8) / 12);
        }
    },
    {   name: "Another swirl",
        producePixel: function() {
            return Math.floor(Lut.sin(Lut.dist(this.dx + this.x1, this.dy + this.y1) * 2) / 16)
                 + Math.floor(Lut.sin(Lut.dist(this.dx + this.x2, this.dy + this.y2) * 4) / 16)
                 + Math.floor(Lut.sin(Lut.dist(this.dx + this.x3, this.dy + this.y3) * 6) / 8)
                 + Math.floor(Lut.sin(Lut.dist(this.dx + this.x4, this.dy + this.y4) * 8) / 8)
                 + 2 * this.angle;
        }
    },
    {   name: "Spiky swirl",
        producePixel: function() {
            return Math.floor(Lut.sin(Lut.dist(this.dx + this.x1, this.dy + this.y1) * 2) / 12)
                 + Math.floor(Lut.sin(Lut.dist(this.dx + this.x2, this.dy + this.y2) * 4) / 12)
                 + Math.floor(Lut.sin(Lut.dist(this.dx + this.x3, this.dy + this.y3) * 6) / 12)
                 + Math.floor(Lut.sin(Lut.dist(this.dx + this.x4, this.dy + this.y4) * 8) / 12)
                 + 4 * this.angle;
        }
    },
    {   name: "Alternative swirl",
        producePixel: function() {
            return Math.floor(Lut.sin(Lut.dist(this.dx + this.x1, this.dy + this.y1) * 2) / 32)
                + Math.floor(Lut.sin(Lut.dist(this.dx + this.x2, this.dy + this.y2) * 4) / 32)
                + Math.floor(Lut.sin(Lut.dist(this.dx + this.x3, this.dy + this.y3) * 6) / 32)
                + Math.floor(Lut.sin(Lut.dist(this.dx + this.x4, this.dy + this.y4) * 8) / 32);
        }
    },
    {   name: "Random Curtain of Rain (in strong wind)",
        producePixel: function(raster) {
            if (this.x === 0 || this.y === 0) {
                return utils.random(16);
            } else {
                return Math.floor((raster[(this.width * this.y) + (this.x - 1)]
                                 + raster[(this.width * (this.y - 1)) + this.x]) / 2)
                     + utils.random(16) - 8;
            }
        }
    },
    {   name: "Heat Burn",
        producePixel: function(raster) {
            if (this.x === 0 || this.y === 0) {
                return utils.random(1024);
            } else {
                return Math.floor(this.dist / 6)
                     + Math.floor((raster[(this.width * this.y) + (this.x - 1)]
                                 + raster[(this.width * (this.y - 1)) + this.x]) / 2)
                     + utils.random(16) - 8;
            }
        }
    },
    {   name: "Evil Mask",
        producePixel: function(raster) {
            return Math.floor(Lut.sin(Lut.dist(this.dx,      this.dy - 20) * 4) / 32) ^
                   Math.floor(Lut.sin(Lut.dist(this.dx + 20, this.dy + 20) * 4) / 32) ^
                   Math.floor(Lut.sin(Lut.dist(this.dx - 20, this.dy + 20) * 4) / 32);
        }
    },
    {   name: "Kaleidoscope",
        producePixel: function(raster) {
            return (this.angle % Math.floor(ANGLE_UNIT / 4)) ^ this.dist;
        }
    },
    {   name: "Quilt",
        producePixel: function(raster) {
            return this.dy ^ this.dx;
        }
    },
    {   name: "Variation on Rain",
        producePixel: function(raster) {
            if (this.x === 0 || this.y === 0) {
                return utils.random(16);
            } else {
                let color = Math.floor((raster[(this.width * this.y) + (this.x - 1)]
                                      + raster[(this.width * (this.y - 1)) + this.x]) / 2);
                color += utils.random(2) - 1;
                if (color < 64) {
                    color += utils.random(16) - 8;
                }
                return color;
            }
        }
    },
    {   name: "Another Variation on Rain",
        producePixel: function(raster) {
            if (this.x === 0 || this.y === 0) {
                return utils.random(16);
            } else {
                let color = Math.floor((raster[(this.width * this.y) + (this.x - 1)]
                                      + raster[(this.width * (this.y - 1)) + this.x]) / 2);
                if (color < 100) {
                    color += utils.random(16) - 8;
                }
                return color;
            }
        }
    },
    {   name: "Variation on Toothed Spiral Sharp",
        producePixel: function(raster) {
            let color = this.angle + Math.floor(Lut.sin(this.dist * 8) / 32);
            this.dx = this.x - this.xcenter;
            this.dy = (this.y - this.ycenter) * 2;
            this.dist = Lut.dist(this.dx, this.dy);
            this.angle = Lut.angle(this.dx, this.dy);
            return Math.floor((color + this.angle
                              + Math.floor(Lut.sin(this.dist * 8) / 32)) / 2);
        }
    },
    {   name: "Another Variation on Toothed Spiral Sharp",
        producePixel: function(raster) {
            let color = this.angle
                + Math.floor(Lut.sin(this.dist * 10) / 16)
                + Math.floor(Lut.cos(Math.floor(this.x * ANGLE_UNIT / width * 2)) / 8)
                + Math.floor(Lut.cos(Math.floor(this.y * ANGLE_UNIT / height * 2)) / 8);
            this.dx = this.x - this.xcenter;
            this.dy = (this.y - this.ycenter) * 2;
            this.dist = Lut.dist(this.dx, this.dy);
            this.angle = Lut.angle(this.dx, this.dy);
            return Math.floor((color + this.angle
                              + Math.floor(Lut.sin(this.dist * 8) / 32)) / 2);
        }
    },
    {   name: "Yet Another Variation on Toothed Spiral Sharp",
        producePixel: function(raster) {
            let color = this.angle
                + Math.floor(Lut.sin(this.dist * 10) / 16)
                + Math.floor(Lut.cos(Math.floor(this.x * ANGLE_UNIT / width * 2)) / 8)
                + Math.floor(Lut.cos(Math.floor(this.y * ANGLE_UNIT / height * 2)) / 8);
            this.dx = this.x - this.xcenter;
            this.dy = (this.y - this.ycenter) * 2;
            this.dist = Lut.dist(this.dx, this.dy);
            this.angle = Lut.angle(this.dx, this.dy);
            return Math.floor((color
                + this.angle
                + Math.floor(Lut.sin(this.dist * 10) / 16)
                + Math.floor(Lut.cos(Math.floor(this.x * ANGLE_UNIT / width * 2)) / 8)
                + Math.floor(Lut.cos(Math.floor(this.y * ANGLE_UNIT / height * 2)) / 8)) / 2);
        }
    },
    {   name: "TV Spiral",
        producePixel: function(raster) {
            if ((this.dy % 2 !== 0)) {
                this.dy *= 2;
                this.dist = Lut.dist(this.dx, this.dy);
                this.angle = Lut.angle(this.dx, this.dy);
            }
            return this.angle + Math.floor(Lut.sin(this.dist * 8) / 32);
        }
    },
    {   name: "Time Tunnel",
        producePixel: function(raster) {
            let color = (this.angle % Math.floor(ANGLE_UNIT / 4)) ^ this.dist;
            this.dx = this.x - this.xcenter;
            this.dy = (this.y - this.ycenter) * 2;
            this.dist = Lut.dist(this.dx, this.dy);
            this.angle = Lut.angle(this.dx, this.dy);
            return Math.floor((color + ((this.angle % Math.floor(ANGLE_UNIT / 4)) ^ this.dist)) / 2);
        }
    },
    {   name: "Variation on Quilt",
        producePixel: function(raster) {
            let color = this.dy ^ this.dx;
            this.dx = this.x - this.xcenter;
            this.dy = (this.y - this.ycenter) * 2;
            return Math.floor((color + (this.dy ^ this.dx)) / 2);
        }
    }
];

exports.descriptions = function(i) {
    return pictures.map((p) => p.name);
};

class Params {
    constructor() {
        this.width = width;
        this.height = height;

        this.xcenter = Math.floor(width/2);
        this.ycenter = Math.floor(height/2);

        this.x1 = utils.random(40) - 20;
        this.x2 = utils.random(40) - 20;
        this.x3 = utils.random(40) - 20;
        this.x4 = utils.random(40) - 20;

        this.y1 = utils.random(40) - 20;
        this.y2 = utils.random(40) - 20;
        this.y3 = utils.random(40) - 20;
        this.y4 = utils.random(40) - 20;
    }

    prepare(x, y) {
        this.x = x;
        this.y = y;

        this.dx = x - this.xcenter;
        this.dy = y - this.ycenter;

        this.dist = Lut.dist(this.dx, this.dy);
        this.angle = Lut.angle(this.dx, this.dy);
    }
}

exports.buildPicture = function(id) {
    let pixelFunc = pictures[id].producePixel;
    if (pixelFunc) {
        let params = new Params();
        let raster = [];
        for (let y=0; y < height; y++) {
            for (let x=0; x < width; x++) {
                params.prepare(x, y);
                let color = normalize(pixelFunc.call(params, raster));
                raster.push(color);
            }
        }
        return raster;
    } else {
        return pictures[id].producePicture();
    }
}

// Fit color value into the palette range using modulo.
function normalize(color) {
    color = color % 255;
    if (color < 0) {
        color += 255;
    }
    return ++color; /* color 0 is never used, so all colors are from 1 through 255 */
}

function noahsFace() {
    const FACE = [
        85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85,
        85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85,
        85, 85, 85, 85, 85, 85,117, 85, 85, 85,117, 85, 85, 85,117, 85, 85,213, 85, 85,
        85, 85, 85, 85, 85, 87,213, 85, 85, 87,213, 85, 85, 87,213, 85, 95, 85, 85, 85,
        85, 85, 85, 85, 85,127,213, 85, 85,127, 85, 85, 85,127, 85, 85,255, 85, 85, 85,
        85, 85, 85, 85, 85,255,213, 85, 85,253, 85, 85, 85,253, 85, 87,255, 85, 85, 85,
        85, 85, 85, 85, 87,247,213, 85, 87,245, 85, 85, 87,245, 85, 95,223, 85, 85, 85,
        85, 85, 85, 85,127,215,213, 85,127,213, 85, 85,127,213, 85,255, 95, 85, 85, 85,
        85, 85, 85, 85,255, 87,213, 85,255, 85, 85, 85,255, 85, 87,253, 95, 85, 85, 85,
        85, 85, 85, 87,253, 87,213, 87,253, 85, 85, 87,253, 85, 95,245, 95, 85, 85, 85,
        85, 85, 85, 95,245, 87,213, 95,245, 85, 85, 95,245, 85,127,213, 95, 85, 85, 85,
        85, 85, 85,127,245, 87,213,127,245, 85, 85,127,245, 85,255,213, 95, 85, 85, 85,
        85, 85, 85,255,255,255,213,255,213, 85, 85,255,213, 87,255, 85, 95, 85, 85, 85,
        85, 85, 85,255,255,255,213,255,213, 85, 85,255,213, 87,255, 85, 95, 85, 85, 85,
        85, 85, 87,255,213, 87,215,255,213, 85, 87,255,213, 95,255, 85, 95, 85, 85, 85,
        85, 85, 87,255,213, 87,215,255,213, 85, 87,255,213, 95,255, 85, 95, 85, 85, 85,
        85, 85, 87,255,213, 87,215,255,213, 85, 87,255,213, 95,255, 85, 95, 85, 85, 85,
        85, 85, 87,255,213, 87,215,255,255,255,215,255,213, 95,255,255,255, 85, 85, 85,
        85, 85, 87,255,213, 87,215,255,255,255,215,255,213, 95,255,255,255, 85, 85, 85,
        85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85,
        85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85,
        170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,
        170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,
        170,170,170,170,170,170,170,170,170,128,  0,  0,  0,170,170,170,170,170,170,170,
        170,170,170,170,170,170,170,168,  0,  0,  0,  0,  0,  0,  2,170,170,170,170,170,
        170,170,170,170,170,170,168,  0,  0,  0,  0,  0,  0,  0,  0,  0,170,170,170,170,
        170,170,170,170,170,170,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 10,170,170,170,
        170,170,170,170,170,160,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 42,170,170,170,
        170,170,170,170,168,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,170,170,170,170,
        170,170,170,170,128,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  2,170,170,170,170,
        170,170,170,170,  0,  0,  0,  0,  0,170,168,  0,  0,  0,  0,  0,170,170,170,170,
        170,170,170,160,  0,  0,  0, 10,170,171,255,252,  0,255,255,  0, 42,170,170,170,
        170,170,170,128,  0,  0, 22,170,171,255,255,255,255,255,255,233, 10,170,170,170,
        170,170,170,  0,  0,  1,106,170,255,255,255,255,255,255,255,255,146,170,170,170,
        170,170,168,  0,  0, 22,170,175,255,255,255,255,255,255,255,255,228,170,170,170,
        170,170,160,  0,  0, 90,170,191,255,255,255,255,255,255,255,255,254,170,170,170,
        170,170,160,  0,  1,106,170,255,255,255,255,255,255,255,255,255,255,170,170,170,
        170,170,160,  0,  5,106,170,255,255,255,255,255,255,255,255,255,255,234,170,170,
        170,170,160,  0,  5,106,170,255,255,255,255,255,255,255,255,255,255,234,170,170,
        170,170,160,  0,  5, 90,170,255,255,255,255,255,255,255,255,255,255,234,170,170,
        170,170,160,  0,  5, 86,170,191,255,255,255,255,255,255,255,255,255,234,170,170,
        170,170,160,  0,  5, 85,170,  0,  0,  3,255,255,255,255,255,255,255,234,170,170,
        170,170,160,  0,  1, 85,  0,  0,  0,  0, 63,255,255,192,  0,  0, 47,234,170,170,
        170,170,160,  0,  0, 80,  2,170,169, 85,107,255,255,  0, 85, 85,  2,170,170,170,
        170,170,160,  0,  0,  0, 22,169, 85, 85, 90,255,253, 86,191,255,226,170,170,170,
        170,170,160,  0,  1, 80, 85, 85, 21, 85, 90,191,253, 85, 69,255,232,170,170,170,
        170,170,134,144,  5, 85, 85, 80,  2,149,106,191,254,149,  0, 90,170,170,170,170,
        170,170,  6, 96, 21, 90,149,104, 10,191,170,191,255,175, 66,165,170,250,170,170,
        170,170,  6,  0, 21,106,170,170,170,255,170,191,255,234,170,170,255,234,170,170,
        170,170,  8, 36, 21,106,191,255,255,255,170,255,255,255,255,255,254,170,170,170,
        170,170,138,170,  5, 90,255,255,255,255,170,255,255,255,255,255,254,170,170,170,
        170,170,138,  2,  0, 90,191,255,255,255,171,255,255,255,255,255,254,170,170,170,
        170,170,133, 40,  0, 22,175,255,255,250,171,255,255,255,255,255,255,170,170,170,
        170,170,160,170,  0, 22,175,255,255,245, 86,175,235,255,255,255,251,170,170,170,
        170,170,168,170,  0, 85,171,255,255,233,106, 86,150,255,255,255,239,170,170,170,
        170,170,170,  1, 65, 85,170,255,255,234,170,175,255,255,255,255,239,170,170,170,
        170,170,170,170,129, 85,106,191,255,234,175,255,255,255,255,255,174,170,170,170,
        170,170,170,170,161, 85,106,171,255,170,255,255,255,255,255,255,170,170,170,170,
        170,170,170,170,160, 85, 90,170,170,165, 85, 85,175,255,255,254,170,170,170,170,
        170,170,170,170,168, 21, 86,170,168,  0,  0,  0,  0, 43,255,250,170,170,170,170,
        170,170,170,170,170,  5, 85,170,170,170,191,255,254,255,255,234,170,170,170,170,
        170,170,170,170,170,160, 85, 90,170,170, 85, 86,170,255,254,170,170,170,170,170,
        170,170,170,170,170,170,  5, 85,170,170,170,170,175,255,250,170,170,170,170,170,
        170,170,170,170,170,170,160, 85,106,175,255,255,255,255,168,170,170,170,170,170,
        170,170,170,170,170,170,170,  5, 90,191,255,255,255,250,138,170,170,170,170,170,
        170,170,170,170,170,170,170,160, 85,175,255,255,255,232,170,170,170,170,170,170,
        170,170,170,170,170,170,170,170,  1,106,191,255,252, 10,170,170,170,170,170,170,
        170,170,170,170,170,170,170,170,168,  0,  0,  0,  2,170,170,170,170,170,170,170,
        170,170,170,170,170,170,170,170,170,160,  0,  0,170,170,170,170,170,170,170,170,
        170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,
        170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,170,
        85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85,
        85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85,
        85, 85, 85, 85,213, 85, 85, 85,117, 85, 85,213, 85, 85, 85,213, 85, 85, 85,213,
        85, 85, 85, 95, 85, 85, 85, 87,213, 85, 95, 85, 85, 85, 95, 85, 85, 85, 95, 85,
        85, 85, 85,253, 85, 85, 85,127, 85, 85,255, 85, 85, 85,255, 85, 85, 85,255, 85,
        85, 85, 87,245, 85, 85, 85,253, 85, 87,255, 85, 85, 87,255, 85, 85, 87,255, 85,
        85, 85, 95,213, 85, 85, 87,245, 85, 95,223, 85, 85, 95,223, 85, 85, 95,223, 85,
        85, 85,255, 85, 85, 85,127,213, 85,255, 95, 85, 85,255, 95, 85, 85,255, 95, 85,
        85, 87,253, 85, 85, 85,255, 85, 87,253, 95, 85, 87,253, 95, 85, 87,253, 95, 85,
        85, 95,245, 85, 85, 87,253, 85, 95,245, 95, 85, 95,245, 95, 85, 95,245, 95, 85,
        85,127,213, 85, 85, 95,245, 85,127,213, 95, 85,127,213, 95, 85,127,213, 95, 85,
        85,255,213, 85, 85,127,245, 85,255,213, 95, 85,255,213, 95, 85,255,213, 95, 85,
        87,255, 85, 85,213,255,213, 87,255,255,255, 87,255,255,255, 87,255,255,255, 85,
        87,255, 85,127, 85,255,213, 87,255,255,255, 87,255,255,255, 87,255,255,255, 85,
        95,255, 87,255,215,255,213, 95,255, 85, 95, 95,255,253, 85, 95,255, 85, 85, 85,
        95,255,127,215,247,255,213, 95,255, 85, 95, 95,255, 95,213, 95,255, 85, 85, 85,
        95,255,253, 85,127,255,213, 95,255, 85, 95, 95,255, 85,253, 95,255, 85, 85, 85,
        95,255,245, 85, 95,255,213, 95,255, 85, 95, 95,255, 85,127, 95,255, 85, 85, 85,
        95,255, 85, 85, 87,255,213, 95,255, 85, 95, 95,255, 85,127, 95,255, 85, 85, 85,
        85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85,
        85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85,
        87,247,255,127, 93, 85, 95,215,245,215, 85,127,245,245, 85,255,117,119,127,245,
        93, 85,117,117,221, 85, 93,119, 93,221, 85, 87, 87, 93, 85,213, 93,215, 87, 85,
        93, 85,117,127, 93, 95,223,215,245,245, 85, 87, 87, 93, 85,253, 87, 87, 87, 85,
        93, 85,117,119, 93, 85, 93,119, 93,221, 85, 87, 87, 93, 85,213, 93,215, 87, 85,
        87,245,117,117,223,213, 95,215, 93,215, 85, 87, 85,245, 85,255,117,119, 87, 85,
        85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85, 85
    ];

    // uncompress bitmap
    let raster = new Array(width * height);
    for (let i=0; i<raster.length; i++) {
        raster[i] = 0;  // Array.fill() is not babelized.
    }

    const x_map = 80;
    const y_map = 98;

    const beg_x = Math.floor((width - 2 * x_map) / 2);
    const beg_y = Math.floor((height - 2 * y_map) / 2);
    const end_x = 2 * x_map + beg_x;
    const end_y = 2 * y_map + beg_y;

    for (let y = beg_y; y < end_y; y += 2) {
        for (let x = beg_x; x < end_x; x += 2) {
            let tx = Math.floor((x - beg_x) / 2);
            let ty = Math.floor((y - beg_y) / 2);

            let bits2 = FACE[Math.floor((x_map * ty + tx) / 4)];

            switch ((x_map * ty + tx) % 4) {
                case 0:
                    bits2 &= 0xc0;
                    bits2 >>= 6;
                    break;

                case 1:
                    bits2 &= 0x30;
                    bits2 >>= 4;
                    break;

                case 2:
                    bits2 &= 0x0c;
                    bits2 >>= 2;
                    break;

                case 3:
                    bits2 &= 0x03;
                    break;
            }

            let color;
            switch (bits2) {
                case 0 :
                    color = utils.random(8) + 192 + 0;
                    raster[width * (y+0) + (x+0)] = color;
                    raster[width * (y+1) + (x+0)] = color;
                    raster[width * (y+0) + (x+1)] = color;
                    raster[width * (y+1) + (x+1)] = color;
                    break;

                case 1 :
                    color = utils.random(8) + 192 + 9;
                    raster[width * (y+0) + (x+0)] = color;
                    raster[width * (y+1) + (x+0)] = color;
                    raster[width * (y+0) + (x+1)] = color;
                    raster[width * (y+1) + (x+1)] = color;
                    break;

                case 2 :
                    color = utils.random(8) + 192 + 17;
                    raster[width * (y+0) + (x+0)] = color;
                    raster[width * (y+1) + (x+0)] = color;
                    raster[width * (y+0) + (x+1)] = color;
                    raster[width * (y+1) + (x+1)] = color;
                    break;

                case 3 :
                    color = utils.random(8) + 192 + 25;
                    raster[width * (y+0) + (x+0)] = color;
                    raster[width * (y+1) + (x+0)] = color;
                    raster[width * (y+0) + (x+1)] = color;
                    raster[width * (y+1) + (x+1)] = color;
                    break;
            }
        }
    }
    return raster;
}
