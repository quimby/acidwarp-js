let utils = require('./utils');

let redRollDirection = false;
let greenRollDirection = false;
let blueRollDirection = false;

let targetPalette,
    workingPalette,
    targetColor;

module.exports = {
    setWorkingPalette: setWorkingPalette,
    getWorkingPalette: getWorkingPalette,
    setTargetPalette: setTargetPalette,
    setFadeColor: setFadeColor,
    roll: roll,
    rollAndFadeOut: rollAndFadeOut,
    rollAndFadeTarget: rollAndFadeTarget
};

function setWorkingPalette(pal) {
    workingPalette = pal;
}

function getWorkingPalette(pal) {
    return workingPalette;
}

function setTargetPalette(pal) {
    targetPalette = pal;
}

function setFadeColor(c) {
    targetColor = c;
}

function roll() {
    maybeInvertSubPalRollDirection();
    rollRgb(workingPalette);
}

function rollAndFadeOut() {
    let numCompleted = fadeColor();
    roll(workingPalette);
    return (numCompleted > 764);
}

function rollAndFadeTarget() {
    let numCompleted = fadeTarget();
    roll(workingPalette);
    rollRgb(targetPalette);
    return (numCompleted > 764);
}


/* fading a palette to the target palette. */
function fadeTarget() {
    let numCompleted = 0;
    for (let i=1; i<256; i++) {
        let color = workingPalette[i];
        let target = targetPalette[i];

        if (color.r === target.r) {
            numCompleted++;
        } else {
            color.r = fadeTo(color.r, target.r);
        }

        if (color.g === target.g) {
            numCompleted++;
        } else {
            color.g = fadeTo(color.g, target.g);
        }

        if (color.b === target.b) {
            numCompleted++;
        } else {
            color.b = fadeTo(color.b, target.b);
        }
    }
    return numCompleted;
}

/* fading a palette to the target color. */
function fadeColor() {
    let numCompleted = 0;
    let target = targetColor;
    for (let i=1; i<256; i++) {
        let color = workingPalette[i];

        if (color.r === target.r) {
            numCompleted++;
        } else {
            color.r = fadeTo(color.r, target.r);
        }

        if (color.g === target.g) {
            numCompleted++;
        } else {
            color.g = fadeTo(color.g, target.g);
        }

        if (color.b === target.b) {
            numCompleted++;
        } else {
            color.b = fadeTo(color.b, target.b);
        }
    }
    return numCompleted;
}

function fadeTo(c, t) {
    return (c < t) ? Math.min(t, c + 8) : Math.max(t, c - 8);
}

/**
 * Randomly changes the individual RGB rotation directions.
 */
function maybeInvertSubPalRollDirection() {
    switch (utils.random(256)) {
        case 0:
            redRollDirection = !redRollDirection;
            console.log("changing red direction");
            break;
        case 1:
            greenRollDirection = !greenRollDirection;
            console.log("changing green direction");
            break;
        case 2:
            blueRollDirection = !blueRollDirection;
            console.log("changing blue direction");
            break;
    }
}

/**
 * Rolls the palette, each color independently.
 * Acidwarp does not change the first color - why?
 */
function rollRgb(pal) {

    let tempRed = pal[redRollDirection ? 255 : 1].r;
    let tempGreen = pal[greenRollDirection ? 255 : 1].g;
    let tempBlue = pal[blueRollDirection ? 255 : 1].b;

    for (let i=1; i<255; i++) {
        if (!redRollDirection)
            pal[i].r = pal[i+1].r;
        else
            pal[256-i].r = pal[255-i].r;

        if (!greenRollDirection)
            pal[i].g = pal[i+1].g;
        else
            pal[256-i].g = pal[255-i].g;

        if (!blueRollDirection)
            pal[i].b = pal[i+1].b;
        else
            pal[256-i].b = pal[255-i].b;
    }

    pal[redRollDirection ? 1 : 255].r = tempRed;
    pal[greenRollDirection ? 1 : 255].g = tempGreen;
    pal[blueRollDirection ? 1 : 255].b = tempBlue;
}
