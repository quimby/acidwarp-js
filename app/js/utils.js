exports.random = function(limit) {
    return Math.floor(Math.random() * limit);
};

exports.color = color;
exports.BLACK = color(0,0,0);
exports.WHITE = color(255,255,255);

function color(r, g, b) {
    return {r: r, g: g, b: b};
}